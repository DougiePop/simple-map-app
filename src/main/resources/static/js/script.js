/* global $, document, ol */
$(document).ready(() => {
  const view = new ol.View({
    center: [-8101767, 3823912],
    zoom: 4,
  });

  const map = new ol.Map({
    controls: ol.control.defaults().extend([
      new ol.control.FullScreen({
        source: 'fullscreen',
      }),
    ]),
    layers: [
        new ol.layer.Tile({
          visible: true,
          preload: Infinity,
          source: new ol.source.BingMaps({
              key: 'AsJ83NnvqsJdSmURSKt3ySErannO79P9g0V-AeoA1Ojc8dc60ogLqMtb1FL2DQrG',
              imagerySet: 'Aerial'
          })
      })
    ],
    target: 'map',
    view,
  });

});
